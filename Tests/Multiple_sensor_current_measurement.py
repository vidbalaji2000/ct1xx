# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 13:48:07 2019

@author: vbalaji
This script is used to calculate the sensitivity matrix (V/I) for a double sensor board with two busbars.
Each sensor is located above a busbar and a current sweep is performed consecutively through the two busbars.
The sensor outputs (voltages) are plotted versus the current supplied and the best fit lines evaluated. The slope
of the lines provide the sensitivity matrix elements. With the sensitivity matrix, the current corresponding to any 
sensor output voltage can be calculated.

"""

import time, os, sys, math, numpy as np, matplotlib.pyplot as plt
import re
import random
from connect import connect
from hp6652a import hp6652a
from hp6032a import hp6032a
from keysightN5748A import keysightN5748A
from agilent34410a import agilent34410a
from excelClass import excelClass
from F_createPathStr import F_createPathStr
from F_createDir import F_createDir


dvm1Addr = 'GPIB0::20::INSTR'
dvm2Addr = 'GPIB0::22::INSTR'
N5747AAddr = 'GPIB0::7::INSTR'
hpsupply2Addr = 'GPIB0::19::INSTR'

#######################################################################
#         Fill the Device information              #
#######################################################################
#Device 
Device = "CT100"
I1_1Array = [];
I2_1Array = [];
Vout1_1Array = [];
Vout2_1Array = [];

I1_2Array = [];
I2_2Array = [];
Vout1_2Array = [];
Vout2_2Array = [];

Curr1_meas = [];
Curr2_meas = [];
Curr_diff = [];

Curr1_calc_wo_CC = [];
Curr2_calc_wo_CC = [];

Curr1_calc_with_CC = [];
Curr2_calc_with_CC = [];

Error1Array_1x1 = [];
Error2Array_1x1 = [];
Error1Array_2x2 = [];
Error2Array_2x2 = [];
RelErr1Array_1x1 = [];
RelErr2Array_1x1 = [];
RelErr1Array_2x2 = [];
RelErr2Array_2x2 = [];
FS_Err1Array_1x1 = [];
FS_Err1Array_2x2 = [];
FS_Err2Array_1x1 = [];
FS_Err2Array_2x2 = [];

CurrArray = [1,2,3,4,5,6,7,8];
FS_val = 8;
nCurrArray = len(CurrArray);
equipDelay = 1.25;

# Line of best fit: y=mx+b where m = (sum(yixi)-mean(y)*sum(xi))/(sum(xi^2)-mean(x)*sum(xi)) and b = mean(y)*sum(xi^2)-mean(x)*sum(yixi)/(sum(xi^2)-mean(x)*sum(xi))
def best_fit_line(x,y):
    denom = x.dot(x) - x.mean() * x.sum()
    m = (x.dot(y) - y.mean()*x.sum())/denom
    print('Slope of line:{}'.format(m))
    b = (y.mean()*x.dot(x) - x.mean()*x.dot(y))/denom
    y_pred = m*x + b;
    plt.scatter(x,y)
    plt.plot(x,y_pred,'r')
    return m

dvm1Port = connect(dvm1Addr);
dvm1 = agilent34410a(dvm1Port);
dvm1.reset();  
dvm1.clrError();                         # clears any exisiting DVM errors
dvm1.configure.voltageDc();              # sets voltage mode
dvm1.beeperOff();


dvm1.trigger.source.immediate();         # set internal signal as trigger
dvm1.trigger.delay.minimum();
dvm1.trigger.sampleCount(10);            # sets sample count to 10


dvm2Port = connect(dvm2Addr);
dvm2 = agilent34410a(dvm2Port);
dvm2.reset();  
dvm2.clrError();                         # clears any exisiting DVM errors
dvm2.configure.voltageDc();              # sets current mode
dvm2.beeperOff();


dvm2.trigger.source.immediate();         # set internal signal as trigger
dvm2.trigger.delay.minimum();
dvm2.trigger.sampleCount(10);            # sets sample count to 10


ps1Port = connect(N5747AAddr);
ps1 = keysightN5748A(ps1Port);

ps1.output.off();             # turn output off

ps1.source.set.voltage(0);    # set voltage limit to 0V
ps1.source.set.current(0);    # set current limit to 0A

ps1.output.on();              # turn output on


ps2Port = connect(hpsupply2Addr);
ps2 = hp6032a(ps2Port);

ps2.output.off();             # turn output off

ps2.source.set.voltage(0);    # set voltage limit to 0V
ps2.source.set.current(0);    # set current limit to 0A

ps2.output.on();              # turn output on

subDir = 'C:/Multiple_Sensors_I_meas/Results/Sensors_Changed' + Device;

dirName = F_createPathStr(subDir);

# Create Directory (if needed)
F_createDir(dirName);

excelFileName = Device + '_Multiple_Sensors_IV_meas_10_random_currents_NI1_B1_I2_B2_wo_resistors_iter3.xlsx';

# Save Results in Excel File
objCheck = 'excelObj' in locals();
    
if not objCheck:
    excelObj = excelClass();
    excelObj.createExcel(dirName, excelFileName);
        
try:    
    excelObj.setActiveSheet();
except:
    excelObj.addSheet(Device);
    excelObj.setActiveSheet(Device);

ps1.source.set.current(0);
ps2.source.set.current(0);
time.sleep(equipDelay);  
print("Read Sensor1 Zero Output Voltage\n")
[minRead, zero_out_v1, maxRead] = dvm1.measure.voltage.dc.averageDvm(10);
print("Read Sensor2 Zero Output Voltage\n")
[minRead, zero_out_v2, maxRead] = dvm2.measure.voltage.dc.averageDvm(10);
print("Zero Current Output Voltage1:"+str(zero_out_v1)+" V")
print("Zero Current Output Voltage2:"+str(zero_out_v2)+" V")

#Apply current sweep to first busbar, keeping current through second busbar zero
for k in range(nCurrArray):
    curr_val = CurrArray[k]
    ps1.source.set.current(curr_val);
    ps1.source.set.voltage(curr_val*2.0); #specific relation between I and V setting for 6652a supply
    time.sleep(equipDelay);  
    print("Read Sensor1 Output Voltage\n")
    [minRead, out_v1, maxRead] = dvm1.measure.voltage.dc.averageDvm(10);
    print("Read Sensor2 Output Voltage\n")
    [minRead, out_v2, maxRead] = dvm2.measure.voltage.dc.averageDvm(10);
    print("Output Voltage1:"+str(out_v1)+" V")
    print("Output Voltage2:"+str(out_v2)+" V")
    Vout1_1Array.append(out_v1);
    Vout2_1Array.append(out_v2);
    cmd2 = 'IOUT?';
    curr1_str = ps1.measure.outputCurrent();   # measures output current
    curr2_str = ps2.rawRead(cmd2);   # measures output current
    curr2_val = [float(s) for s in re.findall(r"[\d*\.\d+]+", curr2_str)]
    I1_1Array.append(float(curr1_str));
    I2_1Array.append(curr2_val[0]);
    
excelObj.addColumn('I1_1 [A]', I1_1Array);
excelObj.addColumn('I2_1 [A]', I2_1Array);
excelObj.addColumn('V1_1 [V]', Vout1_1Array);
excelObj.addColumn('V2_1 [V]', Vout2_1Array);   

ps1.source.set.current(0);    # set current limit to 0

# calculate slope using best fit line approach
I1_1nd=np.asarray(I1_1Array)
Vout1_1nd=np.asarray(Vout1_1Array)
Vout2_1nd=np.asarray(Vout2_1Array)
alpha_bfl_11 = best_fit_line(I1_1nd,Vout1_1nd)    
alpha_bfl_21 = best_fit_line(I1_1nd,Vout2_1nd)    
 
#Apply current sweep to second busbar, keeping current through first busbar zero   
for k in range(nCurrArray):
    curr_val = CurrArray[k]
    ps2.source.set.current(curr_val);
    ps2.source.set.voltage(curr_val*2.0);#specific relation between I and V setting for 6032a supply
    time.sleep(equipDelay);  
    print("Read Sensor1 Output Voltage\n")
    [minRead, out_v1, maxRead] = dvm1.measure.voltage.dc.averageDvm(10);
    print("Read Sensor2 Output Voltage\n")
    [minRead, out_v2, maxRead] = dvm2.measure.voltage.dc.averageDvm(10);
    print("Output Voltage1:"+str(out_v1)+" V")
    print("Output Voltage2:"+str(out_v2)+" V")
    Vout1_2Array.append(out_v1);
    Vout2_2Array.append(out_v2);
    cmd2 = 'IOUT?';
    curr1_str = ps1.measure.outputCurrent();   # measures output current
    curr2_str = ps2.rawRead(cmd2);   # measures output current
    curr2_val = [float(s) for s in re.findall(r"[\d*\.\d+]+", curr2_str)]
    I1_2Array.append(float(curr1_str));
    I2_2Array.append(curr2_val[0]);
    
excelObj.addColumn('I1_2 [A]', I1_2Array);
excelObj.addColumn('I2_2 [A]', I2_2Array);
excelObj.addColumn('V1_2 [V]', Vout1_2Array);
excelObj.addColumn('V2_2 [V]', Vout2_2Array); 

ps2.source.set.current(0);    # set current limit to 8A
    
# calculate slope using best fit line approach
I2_2nd=np.asarray(I2_2Array)
Vout1_2nd=np.asarray(Vout1_2Array)
Vout2_2nd=np.asarray(Vout2_2Array)
alpha_bfl_12 = best_fit_line(I2_2nd,Vout1_2nd)    
alpha_bfl_22 = best_fit_line(I2_2nd,Vout2_2nd)    
   
print("Alpha values calculated using best fit for VI line:\n")
print("Alpha11: "+str(alpha_bfl_11)+" V/A \n")
print("Alpha12: "+str(alpha_bfl_12)+" V/A \n")
print("Alpha21: "+str(alpha_bfl_21)+" V/A \n")
print("Alpha22: "+str(alpha_bfl_22)+" V/A \n")

# calculate slope using voltage matrix corresponding to highest current passed through busbar
alpha11 = (Vout1_1Array[7]-zero_out_v1)/8;
alpha12 = (Vout1_2Array[7]-zero_out_v1)/8;
alpha21 = (Vout2_1Array[7]-zero_out_v2)/8;
alpha22 = (Vout2_2Array[7]-zero_out_v2)/8;

v11 = Vout1_1Array[0]-zero_out_v1
v21 = Vout2_1Array[0]-zero_out_v2
# input two matrices 
alphamat_I7 = np.matrix([[alpha11,alpha12],[alpha21,alpha22]]) 
vmat =np.matrix([[v11],[v21]]) 
  
alphainv_I7 = alphamat_I7.I
# This will return dot product 
current_mat = np.dot(alphainv_I7,vmat) 

print("Current values calculated using alpha matrix:\n")
print("I1:"+str(current_mat[0])+" A\n")
print("I2:"+str(current_mat[1])+" A\n")

print("Current values captured from DMM:\n")
print("I1:"+str(I1_1Array[0])+" A\n")
print("I2:"+str(I2_1Array[0])+" A\n")

#Calculate alpha matrix with cross-coupling coefficients
alphamat_CC = np.matrix([[alpha_bfl_11,alpha_bfl_12],[alpha_bfl_21,alpha_bfl_22]]) 
alphainv_CC = alphamat_CC.I


#Supply random currents, calculate the current supplied from alpha matrix and compare with values read from power sources through DMM to find percentage error
print("Apply random currents\n")
for x in range(10):
   y1=round(random.uniform(1.0,10.0),2)
   ps1.source.set.current(y1);
   ps1.source.set.voltage(y1*2.0);
   ps1.output.on();             # turn output off for power supply 1
   y2=round(random.uniform(1.0,10.0),2)
   ps2.source.set.current(y2);
   ps2.source.set.voltage(y2*2);
   ps2.output.on();             # turn output off for power supply 2   
   time.sleep(equipDelay);  
   [minRead, out_v1, maxRead] = dvm1.measure.voltage.dc.averageDvm(10);
   [minRead, out_v2, maxRead] = dvm2.measure.voltage.dc.averageDvm(10);
   v1 = out_v1 - zero_out_v1
   v2 = out_v2 - zero_out_v2
   vmat_measured =np.matrix([[v1],[v2]]) 
   print("Calculate current values using alpha matrix 2x2 based on voltage measured:\n")
   current_mat_measured = np.dot(alphainv_CC,vmat_measured) 
   print('Current1 calculated:{}\n'.format(current_mat_measured[0]))
   print('Current2 calculated:{}\n'.format(current_mat_measured[1]))
   Curr1_calc_with_CC.append(current_mat_measured[0]);
   Curr2_calc_with_CC.append(current_mat_measured[1]);
   #Calculate current without cross-coupling coefficients
   print("Calculate current values using alpha11 and alpha22 (cross-coupling neglected) based on voltage measured:\n")
   current_measured_0 = v1/alpha_bfl_11
   current_measured_1 = v2/alpha_bfl_22
   print('Current1 calculated with cross-coupling neglected:{}\n'.format(current_measured_0))
   print('Current2 calculated with cross-coupling neglected:{}\n'.format(current_measured_1))
   Curr1_calc_wo_CC.append(current_measured_0);
   Curr2_calc_wo_CC.append(current_measured_1);
   cmd2 = 'IOUT?';
   curr1_str = ps1.measure.outputCurrent();   # measures output current
   curr2_str = ps2.rawRead(cmd2);  # measures output current
   ps1.output.off();             # turn output off for power supply 1
   ps2.output.off();             # turn output off for power supply 2    
   curr2_val = [float(s) for s in re.findall(r"[\d*\.\d+]+", curr2_str)]
   print("Calculate Percent Error\n")
   curr1_mag = abs(float(curr1_str))
   curr2_mag = abs(curr2_val[0])
   curr_diff = abs(curr1_mag-curr2_mag)
   print('Current1 measured:{}\n'.format(curr1_mag))
   print('Current2 measured:{}\n'.format(curr2_mag))
   Curr1_meas.append(curr1_mag);
   Curr2_meas.append(curr2_mag);
   Curr_diff.append(curr_diff);

   if (curr1_mag == 0 or (curr1_mag<0 and int(str(curr1_mag).split('.')[1][0])== 0)):
       error1_1x1 = 0.0
       error1_2x2 = 0.0
       relative_error1_1x1 = 0.0
       relative_error1_2x2 = 0.0

   else:
       error1_1x1 = abs(abs(current_measured_0)-curr1_mag)
       error1_2x2 = abs(abs(current_mat_measured[0])-curr1_mag)
       relative_error1_1x1 = float(error1_1x1)/curr1_mag
       relative_error1_2x2 = float(error1_2x2)/curr1_mag

   FS_error1_1x1 = (float(abs(abs(current_measured_0)-curr1_mag))/FS_val)*100
   FS_error1_2x2 = (float(abs(abs(current_mat_measured[0])-curr1_mag))/FS_val)*100
   
   if (curr2_mag == 0 or (curr2_mag<0 and int(str(curr2_mag).split('.')[1][0])== 0)):
       error2_1x1 = 0.0
       error2_2x2 = 0.0
       relative_error2_1x1 = 0.0
       relative_error2_2x2 = 0.0

   else:
       error2_1x1 = abs(abs(current_measured_1)-curr2_mag)
       error2_2x2 = abs(abs(current_mat_measured[1])-curr2_mag)
       relative_error2_1x1 = float(error2_1x1)/curr2_mag
       relative_error2_2x2 = float(error2_2x2)/curr2_mag
      
   FS_error2_1x1 = (float(abs(abs(current_measured_1)-curr2_mag))/FS_val)*100
   FS_error2_2x2 = (float(abs(abs(current_mat_measured[1])-curr2_mag))/FS_val)*100     
        

   percent_error1_1x1 = relative_error1_1x1*100
   percent_error2_1x1 = relative_error2_1x1*100
   percent_error1_2x2 = relative_error1_2x2*100
   percent_error2_2x2 = relative_error2_2x2*100
   Error1Array_1x1.append(percent_error1_1x1);
   Error2Array_1x1.append(percent_error2_1x1);
   Error1Array_2x2.append(percent_error1_2x2);
   Error2Array_2x2.append(percent_error2_2x2);
   RelErr1Array_1x1.append(relative_error1_1x1)
   RelErr2Array_1x1.append(relative_error2_1x1)
   RelErr1Array_2x2.append(relative_error1_2x2)
   RelErr2Array_2x2.append(relative_error2_2x2)
   FS_Err1Array_1x1.append(FS_error1_1x1)
   FS_Err1Array_2x2.append(FS_error1_2x2)
   FS_Err2Array_1x1.append(FS_error2_1x1)
   FS_Err2Array_2x2.append(FS_error2_2x2)

ps1.output.off();             # turn output off for power supply 1
ps2.output.off();             # turn output off for power supply 2     
SumRelError1_1x1 = sum(RelErr1Array_1x1)  
SumRelError2_1x1 = sum(RelErr2Array_1x1)  
SumRelError1_2x2 = sum(RelErr1Array_2x2)  
SumRelError2_2x2 = sum(RelErr2Array_2x2)  
MPE1_1x1 = (100/len(RelErr1Array_1x1))*SumRelError1_1x1
MPE2_1x1 = (100/len(RelErr2Array_1x1))*SumRelError2_1x1
MPE1_2x2  = (100/len(RelErr1Array_2x2 ))*SumRelError1_2x2 
MPE2_2x2  = (100/len(RelErr2Array_2x2 ))*SumRelError2_2x2 

print('Maximum percent error in I1 calculation using alpha11 and alpha22 is: ', max(Error1Array_1x1))   
print('Maximum percent error in I2 calculation using alpha11 and alpha22 is: ', max(Error2Array_1x1)) 
print('Mean percentage error in I1 calculation using alpha11 and alpha22 is: ', MPE1_1x1)   
print('Mean percentage error in I2 calculation using alpha11 and alpha22 is: ', MPE2_1x1) 
print('Maximum percent error in I1 calculation using alpha matrix 2x2 is: ', max(Error1Array_2x2))   
print('Maximum percent error in I2 calculation using alpha matrix 2x2 is: ', max(Error2Array_2x2)) 
print('Mean percentage error in I1 calculation using alpha matrix 2x2 is: ', MPE1_2x2)   
print('Mean percentage error in I2 calculation using alpha matrix 2x2 is: ', MPE2_2x2) 

print('FS Error I1 using alpha11 & 22 [%] ', FS_Err1Array_1x1)   
print('FS Error I2 using alpha11 & 22 [%] ', FS_Err2Array_1x1) 
print('FS Error I1 using alpha 2x2 [%] ', FS_Err1Array_2x2)   
print('FS Error I2 using alpha 2x2 [%] ', FS_Err2Array_2x2) 

excelObj.addColumn('Current 1 measured [A]', Curr1_meas);
excelObj.addColumn('Current 2 measured [A]', Curr2_meas);
excelObj.addColumn('Current Difference [A]', Curr_diff);
excelObj.addColumn('Percent Error I1 using alpha11 & 22 [%]', Error1Array_1x1);
excelObj.addColumn('Percent Error I2 using alpha11 & 22 [%]', Error2Array_1x1);
excelObj.addColumn('Percent Error I1 using alpha 2x2 [%]', Error1Array_2x2);
excelObj.addColumn('Percent Error I2 using alpha 2x2 [%]', Error2Array_2x2); 
excelObj.addColumn('FS Error I1 using alpha11 & 22 [%]', FS_Err1Array_1x1);
excelObj.addColumn('FS Error I2 using alpha11 & 22 [%]', FS_Err2Array_1x1);
excelObj.addColumn('FS Error I1 using alpha 2x2 [%]', FS_Err1Array_2x2);
excelObj.addColumn('FS Error I2 using alpha 2x2 [%]', FS_Err2Array_2x2); 

excelObj.saveExcel();
            
del(excelObj);  
